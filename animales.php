<?php
    header('Content-Type: text/html; charset=ISO-8859-1');
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <title>OASIS - Animales</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="misEstilos.css" media="screen" />


</head>

<body>
    <?php 
    session_start(); 
    include("datosconexion.php");
    
    ?>

    <section class="container" id="cuerpo">

        <!-- Imagen cabecera -->
        <section class="container slider" id="cabecera1"></section>

        <!--MENU-->
        <nav class="navbar navbar-light navbar-expand-md sticky-top" style="background-color: #0F7304;">

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                <div class="navbar-nav mr-auto ml-auto text-center" id="enlaces">
                    <a class="nav-item nav-link" href="index.php">INICIO</a>
                    <a class="nav-item nav-link active" href="animales.php">ANIMALES</a>
                    <!--Si la sesion esta iniciada y el usuario tiene rol 'usuario' que me lleve a frontend.php, si la sesion esta iniciada y el usuario tiene rol 'admin' que me lleve a backend.php, si la sesion no esta iniciada que me lleve al login-->
                    <?php 
                    if(isset($_SESSION['reg'])){
                        $reg=$_SESSION['reg'];
                        if($reg['rol']== 'usuario'){ ?>
                            <a class="nav-item nav-link" href="frontend.php">USUARIOS</a>
                  <?php }else if($reg['rol']== 'admin'){ ?>
                            <a class="nav-item nav-link" href="backend.php">USUARIOS</a>
                  <?php } 
                    
                    }else{ ?>
                        <a class="nav-item nav-link" href="entrar.php">USUARIOS</a>
              <?php }  ?>
                </div>
            </div>
        </nav>

        <!--CUERPO DE LA PAGINA-->
        <?php
        //CONEXION BASE DE DATOS
        $conexion = mysqli_connect(HOST, USER, PASSWORD, DATABASE);
        if(mysqli_connect_errno()){
            echo "Fallo al conectar con la BBDD";
            exit();
        }
        
        //PARA MOSTRAR LA INFORMACION DE LOS ANIMALES
        $consulta1= "SELECT foto, especie, nombre, sexo, fnto, caracter, problemas, historia FROM animales";
        $resultado1=mysqli_query($conexion, $consulta1);

        if (mysqli_num_rows($resultado1) > 0) {
            while($fila=mysqli_fetch_row($resultado1)){
                $resultadoSQL[]=$fila;
            }


        }else{
            $mensaje="Todavia no hay animales.";
        }
/*------------------------------------------------------------------------------------------------------------------------*/

        ?>


        <!--VISUALIZAR IMAGENES Y DATOS-->
        <div class="container">
            <h1>
                <p class="mt-5 mb-3 text-center font-weight-bold ">ANIMALES</p>
            </h1>


            <div class="text-center" id="animales">
                <?php if(isset($resultadoSQL) && is_array($resultadoSQL)){ ?>
                <?php foreach( $resultadoSQL as $linea ){ ?>
                <img src="/OASIS/imagenes/<?php echo $linea[0];?>" width="80%" /><br>
                <?php echo "<strong>Especie: </strong>".$linea[1]."<br><strong>Nombre: </strong>". $linea[2]."<br><strong>Sexo: </strong>". $linea[3]."<br><strong>Fecha nacimiento: </strong>". $linea[4]."<br><strong>Caracter: </strong>". $linea[5]."<br><strong>Problemas: </strong>". $linea[6]."<br><strong>Historia: </strong>". $linea[7]; ?>
                <br><br><hr noshade="noshade" style="width:100%"><br>
                <?php } ?>
                <?php } ?>
                
                <!--Sale este mensaje si todavia no hay animales-->
                <?php if(isset($mensaje)){echo $mensaje;} ?>

            </div>


        </div>


        <!--FOOTER-->
        <div id="footer">
            <a href="https://es-es.facebook.com/"><img src="imagenes/faceicon.png" /></a>
            <a href="https://twitter.com/?lang=es"><img src="imagenes/twittericon.png" /></a>
            <a href="https://www.instagram.com/?hl=es"><img src="imagenes/instaicon.png" /></a>
        </div>


    </section>



    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>





</body>

</html>
