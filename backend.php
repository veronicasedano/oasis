<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>OASIS - Admin</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="misEstilos.css" media="screen" />


</head>

<body>
    <?php 
    session_start();
    include("datosconexion.php");
    $reg=$_SESSION['reg'];
    
    /*Conexion con la base de datos*/
    $conexion = mysqli_connect(HOST, USER, PASSWORD, DATABASE);
    if(mysqli_connect_errno()){
        echo "Fallo al conectar con la BBDD";
        exit();
    }
    
    ?>

    <section class="container" id="cuerpo">

        <!-- Imagen cabecera -->
        <section class="container slider" id="cabecera1"></section>

        <!--MENU-->
        <nav class="navbar navbar-light navbar-expand-sm sticky-top" style="background-color: #0F7304;">

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                <div class="navbar-nav mr-auto ml-auto text-center" id="enlaces">
                    <a class="nav-item nav-link" href="index.php">INICIO</a>
                    <a class="nav-item nav-link" href="animales.php">ANIMALES</a>
                    <a class="nav-item nav-link active" href="backend.php">USUARIOS</a>

                </div>
            </div>
        </nav>

        <!--CUERPO DE LA PAGINA-->

        <h1>
            <p class="mt-5 text-center font-weight-bold "><?php echo "¡Hola ".$reg['nombre']."!"; ?></p>
        </h1>
        <div class="container">
            <!--Fila para mostrar los datos de perfil-->
            <div class="row d-flex justify-content-center">
                <!--Tabla que muestra los datos de perfil del usuario-->
                <table class="table table-responsive" id="tabla">
                    <thead>
                        <tr>
                            <th colspan="5">DATOS PERFIL</th>
                        </tr>
                        <tr>
                            <th>DNI</th>
                            <th>NOMBRE</th>
                            <th>CORREO</th>
                            <th>TELEFONO</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            /*Coger los datos del usuario y mostrarlos en la tabla*/
                            $sql="SELECT * FROM usuarios WHERE dni='".$reg["dni"]."'";
                            $result=mysqli_query($conexion, $sql);
                            while($row=mysqli_fetch_row($result)){
                                echo "<tr>
                                <td>$row[0]</td>
                                <td>$row[1]</td>
                                <td>$row[2]</td>
                                <td>$row[4]</td>
                                </tr>";
                            
                            } 
                            
                            ?>
                    </tbody>

                </table>

            </div>
            <!--Fila para el boton de añadir animales, boton modificar datos perfil, formulario añadir animales y formulario de modificar datos de perfil-->
            <div class="row">
                <!--Boton añadir animal-->
                <div class="col-md mt-5 text-center">
                    <button type="button" class="btn btn-success btn-lg" id="aniadir" onClick="document.getElementById('formaniadir').classList.remove('formoculto')">Añadir animal</button>
                    <br><br><br>
                    <!--Formulario para añadir animales-->
                    <form method='POST' action='' id="formaniadir" class="formoculto" enctype="multipart/form-data">
                        <h2 id="titulo">AÑADIR ANIMAL</h2>
                        <label><strong>ID ANIMAL</strong></label><br>
                        <input type='text' name='id_animal' id="input" required><br>
                        <label><strong>ESPECIE</strong></label><br>
                        <input type='text' name='especie' id="input" required><br>
                        <label><strong>NOMBRE</strong></label><br>
                        <input type='text' name='nombre' id="input" required><br>
                        <label><strong>SEXO</strong></label><br>
                        <input type='text' name='sexo' id="input" required><br>
                        <label><strong>FECHA NACIMIENTO</strong></label><br>
                        <input type='date' name='fnto' id="input" required><br>
                        <label><strong>CARACTER</strong></label><br>
                        <textarea name='caracter' id="input" required></textarea><br>
                        <label><strong>PROBLEMAS</strong></label><br>
                        <textarea name='problemas' id="input" required></textarea><br>
                        <label><strong>HISTORIA</strong></label><br>
                        <textarea name='historia' id="input" required></textarea><br>
                        <label><strong>FOTO</strong></label><br>
                        <label for="imagen"></label><input type="file" name="foto" size="50" id="input" required><br><br>
                        <input type='submit' name='alta' id="boton">
                    </form>

                    <?php
                    if(isset($_POST["alta"])){
                        //GUARDAR LA FOTO EN EL SERVIDOR
                        //Recibimos los datos de la imagen
                        $nombre_imagen=$_FILES['foto']['name']; //guarda la imagen y el nombre de la imagen
                        $tipo_imagen=$_FILES['foto']['type'];//guarda la imagen y el tipo de la imagen
                        $tamagno_imagen=$_FILES['foto']['size']; //guarda la imagen y el tamaño de la imagen


                        //Para controlar el tamaño de la imagen
                        if($tamagno_imagen<= 1000000){ // 1 millon de bytes es aprox 1 MB
                            if($tipo_imagen="image/jpeg" || $tipo_imagen="image/jpg" || $tipo_imagen="image/png" || $tipo_imagen="image/gif"){
                                //Directorio donde queremos guardar la imagen
                                //$_SERVER['DOCUMENT_ROOT'] --> es la carpeta htdocs de xampp
                                $carpeta_destino=$_SERVER['DOCUMENT_ROOT'] . '/oasis/imagenes/';
                                //Mover la imagen de la carpeta temporal a la que hemos escogido en el paso anterior
                                move_uploaded_file($_FILES['foto']['tmp_name'] , $carpeta_destino.$nombre_imagen);
                            }else{
                                //Si no es una imagen
                                echo "Solo se pueden subir .jpeg .jpg .png y .gif";
                            }


                        }else{
                            //Si el tamaño de la imagen es demasiado grande
                            echo "La imagen es demasiado grande";
                        }
                        
                        //INSERTAMOS LOS DATOS EN LA BASE DE DATOS
                        $query = "INSERT INTO animales
                        VALUES ('" . $_POST["id_animal"] ."','" . $_POST["especie"]."','" . $_POST["nombre"]."','" . $_POST["sexo"]."','" . $_POST["fnto"]."','" . $_POST["caracter"]."','" . $_POST["problemas"]."','" . $_POST["historia"]."','" . $nombre_imagen."')";
                        mysqli_query($conexion, $query) or die(mysqli_error($conexion));
                        echo "Animal añadido correctamente.";
                        }
                    
                    ?>
                </div>
                <div class="col-md mt-5 text-center">
                    <!--Boton para modificar datos perfil-->
                    <button type="button" class="btn btn-success btn-lg" id="modificar" onClick="document.getElementById('mi-formulario').classList.remove('formoculto')">Modificar datos perfil</button>
                    <br><br><br>
                    <!--Formulario de modificar datos de perfil-->
                    <form method='post' action='' class='formoculto' id='mi-formulario'>
                        <h2 id="titulo">MODIFICACIONES</h2>
                        <label><strong>CORREO</strong></label><br>
                        <input type='text' name='correo' id="input"><br>
                        <label><strong>CONTRASEÑA</strong></label><br>
                        <input type='password' name='password' id="input" data-type="password"><br>
                        <label><strong>TELEFONO</strong></label><br>
                        <input type='text' name='telefono' id="input"><br><br>
                        <input type='submit' name='mod2' id="boton" value="ENVIAR">
                    </form>


                    <?php
                        if(isset($_POST["mod2"])){
                            if(isset($_POST["correo"]) && $_POST["correo"] != ""){
                                $query1 = "UPDATE usuarios SET correo='".$_POST["correo"]."' WHERE dni='".$reg["dni"]."'";
                                mysqli_query($conexion, $query1) or die(mysqli_error($conexion));
                                echo "Correo modificado correctamente.<br>";
                                
                            }
                            if(isset($_POST["password"]) && $_POST["password"] != ""){
                                $query2 = "UPDATE usuarios SET password='".$_POST["password"]."' WHERE dni='".$reg["dni"]."'";
                                mysqli_query($conexion, $query2) or die(mysqli_error($conexion));
                                echo "Contraseña modificada correctamente.<br>";
                                
                            }
                            if(isset($_POST["telefono"]) && $_POST["telefono"] != ""){
                                $query3 = "UPDATE usuarios SET telefono='".$_POST["telefono"]."' WHERE dni='".$reg["dni"]."'";
                                mysqli_query($conexion, $query3) or die(mysqli_error($conexion));
                                echo "Teléfono modificado correctamente.<br>";
                                
                            }
                            
                         
                        }
                    ?>


                </div>

            </div>


            <!--Fila para el enlace de Cerrar sesion-->
            <div class="row">
                <div class="col-md mt-5 mb-3 ml-3">
                    <strong><a href="cerrar.php" style="color:#0F7304;">Cerrar sesión</a></strong>
                </div>
            </div>
        </div>


        <!--FOOTER-->
        <div id="footer">
            <a href="https://es-es.facebook.com/"><img src="imagenes/faceicon.png" /></a>
            <a href="https://twitter.com/?lang=es"><img src="imagenes/twittericon.png" /></a>
            <a href="https://www.instagram.com/?hl=es"><img src="imagenes/instaicon.png" /></a>
        </div>


    </section>



    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>






</body>

</html>
