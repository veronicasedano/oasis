<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <title>OASIS - Inicio</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="misEstilos.css" media="screen" />


</head>

<body>
    <?php 
    session_start();
    include("datosconexion.php");
    
    
    ?>

    <section class="container" id="cuerpo">

        <!--Imagen cabecera-->
        <section class="container slider"></section>

        <!--MENU-->
        <nav class="navbar navbar-light navbar-expand-md sticky-top" style="background-color: #0F7304;">

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                <div class="navbar-nav mr-auto ml-auto text-center" id="enlaces">
                    <a class="nav-item nav-link active" href="index.php">INICIO</a>
                    <a class="nav-item nav-link" href="animales.php">ANIMALES</a>
                    <!--Si la sesion esta iniciada y el usuario tiene rol 'usuario' que me lleve a frontend.php, si la sesion esta iniciada y el usuario tiene rol 'admin' que me lleve a backend.php, si la sesion no esta iniciada que me lleve al login-->
                    <?php 
                    if(isset($_SESSION['reg'])){
                        $reg=$_SESSION['reg'];
                        if($reg['rol']== 'usuario'){ ?>
                            <a class="nav-item nav-link" href="frontend.php">USUARIOS</a>
                  <?php }else if($reg['rol']== 'admin'){ ?>
                            <a class="nav-item nav-link" href="backend.php">USUARIOS</a>
                  <?php } 
                    
                    }else{ ?>
                        <a class="nav-item nav-link" href="entrar.php">USUARIOS</a>
              <?php }  ?>



                </div>
            </div>
        </nav>

        <!--CUERPO DE LA PAGINA-->
        <h1>
            <p class="mt-5 mb-5 text-center font-weight-bold ">BIENVENID@</p>
        </h1>
        <img class="float-right mr-5 ml-3" id="rata" src="imagenes/ratainicio.jpg" />

        <p class="text-justify mr-5 ml-5 mb-5">Este es un pequeño proyecto que surgió en Burgos para ayudar a encontrar una nuevo hogar a los animales exóticos que estaban siendo abandonadas en toda España, también para dar la oportunidad de que los usuarios puedan apadrinar animales y ofrecer asesoría sobre las necesidades y cuidados de estos animales para todas aquellas personas que comparten su vida con ellos. Nosotros recogemos animales como hamsters (rusos, sirios, chinos o campbell), ratas domésticas, conejos, cobayas, aves (agapornis, nifas, cacatuas, periquitos, canarios, etc), ratones, chinchillas y jerbos.
            Todos los voluntarios y voluntarias que trabajamos para que esto sea posible tenemos varios animales de acogida en nuestras casas (en funcion de las posibilidades de cada uno). A parte de cuidar a los animales, cuando recibimos avisos de un animal abandonado el voluntario que mas cerca este del lugar va a recogerlo con el material adecuado. A veces se necesita la ayuda de mas de una persona porque hay animales asustadizos y que huyen constantemente. Tambien nos encargamos de responder a todas aquellas personas que están interesadas en adoptar a nuestros animales y valorar si van a estar adecuadamente atendidos o no. Para eso, les hacemos una serie de preguntas y les damos toda la informacion posible respecto a su alimentación, hábitat y demás cuidados que necesitan. Aunque "OASIS" nació en Burgos tenemos voluntarios por toda España (Madrid, Valencia, Jaén, Bilbao y Barcelona)</p>

        <!--BOTON DE DONAR-->
        <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" class="float-right mr-5 ml-4">
            <input type="hidden" name="cmd" value="_s-xclick" />
            <input type="hidden" name="hosted_button_id" value="95SA9EDFHZMWN" />
            <input type="image" src="https://www.paypalobjects.com/es_ES/ES/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Botón Donar con PayPal" />
            <img alt="" border="0" src="https://www.paypal.com/es_ES/i/scr/pixel.gif" width="1" height="1" />
        </form>

        <p class="text-justify mr-5 ml-5 mb-5">Somos una organizacion sin ningún ánimo de lucro que lo único que quiere es ayudar a estos animales que se ven afectados por la irresponsabilidad de algunos humanos. A veces nos es difícil afrontar todos los gastos que suponen mantener a tantos animales ya que no recibimos ninguna ayuda por parte del estado, por lo tanto, agradecemos muchísimo todas las donaciones tanto de dinero como de alimentos, mantas, jaulas y demás. Si quieres contactar con nosotros para hacernos cualquier pregunta sobre como adoptar, apadrinar u otra cuestión, estaremos encantados de contestarte lo antes posible. Te dejamos nuestro e-mail: <strong>asociacionoasis@gmail.com</strong> y número de teléfono: <strong>654266310</strong>.</p>







        <!--FOOTER-->
        <div id="footer">
            <a href="https://es-es.facebook.com/"><img src="imagenes/faceicon.png" /></a>
            <a href="https://twitter.com/?lang=es"><img src="imagenes/twittericon.png" /></a>
            <a href="https://www.instagram.com/?hl=es"><img src="imagenes/instaicon.png" /></a>
        </div>


    </section>



    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>





</body>

</html>
