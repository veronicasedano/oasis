<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <title>OASIS - Registrarse</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="misEstilos.css" media="screen" />

</head>

<body>

    <?php
        /*CODIGO PHP*/
        session_start();
        include("datosconexion.php");
    
    //PARA REGISTRARTE
    if(isset($_POST['registrarse'])){
            if(empty($_POST['dni']) || empty($_POST['nombre']) || empty($_POST['correo']) || empty($_POST['password']) || empty($_POST['telefono'])){
                
                $mensaje2="Debes rellenar todos los campos.";
                
            }else{
                
                $dni=$_POST['dni'];
                $nombre=$_POST['nombre'];
                $correo=$_POST['correo'];
                $password=$_POST['password'];  
                $telefono=$_POST['telefono'];
                //1º Conexion
                $conexion = mysqli_connect(HOST, USER, PASSWORD, DATABASE);
                //control de error
                if(!$conexion){//Si ha habido fallo en la conexion con la base de datos
                    echo "Error en la conexion en la BBDD", mysqli_connect_error();
                    exit();
                }
                //2º Preparacion SQL
                $query="INSERT INTO usuarios VALUES ('" . $_POST["dni"] ."','" . $_POST["nombre"]."','" . $_POST["correo"]."','" . $_POST["password"]."','" . $_POST["telefono"]."','" . "usuario" ."')";
                $resultado=mysqli_query($conexion,$query);
                //control de error
                if(!$resultado){
                    echo "consulta fallida.", mysqli_error($conexion);
                    exit();
                }
      
                $mensaje="¡Te has registrado correctamente! Ahora puedes entrar con tu DNI y contraseña";
                
            }    


        }
       
        
        ?>

    <section class="container" id="cuerpo">

        <!-- Imagen cabecera -->
        <section class="container slider" id="cabecera1"></section>

        <!--MENU-->
        <nav class="navbar navbar-light navbar-expand-sm sticky-top" style="background-color: #0F7304;">

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                <div class="navbar-nav mr-auto ml-auto text-center" id="enlaces">
                    <a class="nav-item nav-link" href="index.php">INICIO</a>
                    <a class="nav-item nav-link" href="animales.php">ANIMALES</a>
                    <a class="nav-item nav-link active" href="registrarse.php">USUARIOS</a>
                </div>
            </div>
        </nav>

        <!--CUERPO DE LA PAGINA-->
        <section id="cuerpo">
            <h1>
                <img class="mx-auto d-block mt-5" id="iconologin" src="imagenes/fotoregistro.png" />
                <!--<p class="mt-5 text-center font-weight-bold ">LOGIN</p>-->
            </h1>
            <form method="post" action="">
                <div class="cuerpoform">
                    <div class="fondoform">
                        <h3 style="color:white"><strong>REGISTRARSE</strong></h3><br>
                        <label><strong>DNI</strong></label><br>
                        <input type="text" name="dni" class="input">
                        <br>
                        <label><strong>NOMBRE</strong></label><br>
                        <input type="text" name="nombre" class="input">
                        <br>
                        <label><strong>CORREO</strong></label><br>
                        <input type="text" name="correo" class="input">
                        <br>
                        <label><strong>CONTRASEÑA</strong></label><br>
                        <input type="password" data-type="password" name="password" class="input">
                        <br>
                        <label><strong>TELÉFONO</strong></label><br>
                        <input type="text" name="telefono" class="input">
                        <br><br>
                        <input type="submit" class="button" value="REGISTRARSE" name="registrarse">
                        <div class="hr"></div>
                        <!--Mensaje "Debes rellenar todos los campos" -->
                        <strong><?php if(isset($mensaje2)){echo $mensaje2;} ?></strong>
                        <!--Mensaje para avisar al nuevo usuario que ya puede entrar -->
                        <strong><?php if(isset($mensaje)){echo $mensaje;} ?></strong>
                    </div>

                </div>


            </form>
        </section>
        
        
        <div class="mt-5 mb-3 ml-3">
            ¿Tienes una cuenta? <strong><a href="entrar.php" style="color:#0F7304;">Entrar</a></strong>
        </div>
        
        
        <!--FOOTER-->
        <div id="footer" class="mt-5">
            <a href="https://es-es.facebook.com/"><img src="imagenes/faceicon.png" /></a>
            <a href="https://twitter.com/?lang=es"><img src="imagenes/twittericon.png" /></a>
            <a href="https://www.instagram.com/?hl=es"><img src="imagenes/instaicon.png" /></a>
        </div>


    </section>


    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>





</body>

</html>
