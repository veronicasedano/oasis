<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <title>OASIS - Entrar</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="misEstilos.css" media="screen" />
    <style>
        body {
            margin: 0;

        }

    </style>

</head>

<body>

    <?php
        /*CODIGO PHP*/
        session_start();
        include("datosconexion.php");
    

        //PARA ENTRAR 
        if(isset($_POST['entrar'])){
            if(empty($_POST['user']) || empty($_POST['pass'])){
                
                $aviso = "Debes rellenar todos los campos.";
                
            }else{
                
                $user=$_POST['user'];
                $pass=$_POST['pass'];  
                //1º Conexion
                $conexion = mysqli_connect(HOST, USER, PASSWORD, DATABASE);
                //control de error
                if(!$conexion){//Si ha habido fallo en la conexion con la base de datos
                    echo "Error en la conexion en la BBDD", mysqli_connect_error();
                    exit();
                }
                //2º Preparacion SQL
                //$hash=md5($pass);
                $sql="SELECT * FROM usuarios WHERE dni='".$user."' and password='".$pass."'";
                //$sql="SELECT * FROM usuarios WHERE dni='$user' and password='$hash'";
                $resultado=mysqli_query($conexion,$sql);
                //control de error
                if(!$resultado){
                    echo "consulta fallida.", mysqli_error($conexion);
                    exit();
                }

                //3º Ejecucion consulta
                if(mysqli_num_rows($resultado)){ // nos dice cuantos registros hay en la consulta
                    //El login es correcto   
                    //4º Proceso de resultado (de datos)
                    $reg=mysqli_fetch_array($resultado);
                    $rol=$reg['rol'];

                    $_SESSION['reg']=$reg;

                    if($rol=='admin'){
                        /*Si entra un administrador lo lleva al backend*/
                        header('Location: backend.php');
                        exit();
                    }else{
                        /*Si entra un usuario normal lo lleva al frontend*/
                        header('Location: frontend.php');
                        exit();
                    }   
                    

                }else{
                    $mensaje4 = "ERROR. El login es incorrecto";
                  
                }
                
            }

        }
    
        
        ?>

    <section class="container" id="cuerpo">

        <!-- Imagen cabecera -->
        <section class="container slider" id="cabecera1"></section>

        <!--MENU-->
        <nav class="navbar navbar-light navbar-expand-sm sticky-top" style="background-color: #0F7304;">

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                <div class="navbar-nav mr-auto ml-auto text-center" id="enlaces">
                    <a class="nav-item nav-link" href="index.php">INICIO</a>
                    <a class="nav-item nav-link" href="animales.php">ANIMALES</a>
                    <a class="nav-item nav-link active" href="entrar.php">USUARIOS</a>
                </div>
            </div>
        </nav>

        <!--CUERPO DE LA PAGINA-->
        <section id="cuerpo">
            <h1>
                <img class="mx-auto d-block mt-5" id="iconologin" src="imagenes/iconologin2.png" />
            </h1>
            <form method="post" action="">
                <div class="cuerpoform">
                    <div class="fondoform">
                        <h3 style="color:white"><strong>ENTRAR</strong></h3><br>
                        <label><strong>DNI</strong></label><br>
                        <input type="text" name="user" class="input">
                        <br>
                        <label><strong>CONTRASEÑA</strong></label><br>
                        <input type="password" data-type="password" name="pass" class="input">
                        <br><br>
                        <input type="submit" class="button" value="ENTRAR" name="entrar" class="input">
                        <div class="hr"></div>
                        <!--Mensaje "Debes relenar todos los campos" -->
                        <strong><?php if(isset($aviso)){echo $aviso;} ?></strong>
                        <!--Mensaje "ERROR. Login incorrecto" -->
                        <strong><?php if(isset($mensaje4)){echo $mensaje4;} ?></strong>
                    </div>

                </div>


            </form>

        </section>

        <div class="mt-5 mb-3 ml-3">
            ¿No tienes una cuenta? <strong><a href="registrarse.php" style="color:#0F7304;">Regístrate</a></strong>
        </div>

        <!--FOOTER-->
        <div id="footer" class="mt-5">
            <a href="https://es-es.facebook.com/"><img src="imagenes/faceicon.png" /></a>
            <a href="https://twitter.com/?lang=es"><img src="imagenes/twittericon.png" /></a>
            <a href="https://www.instagram.com/?hl=es"><img src="imagenes/instaicon.png" /></a>
        </div>


    </section>


    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>





</body>

</html>
