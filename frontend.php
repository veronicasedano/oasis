<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <title>OASIS - Bienvenido</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="misEstilos.css" media="screen" />

</head>

<body>
    <?php
    session_start();
    include("datosconexion.php");
    $reg=$_SESSION['reg'];
    
    
    /*Conexion con la base de datos*/
    $conexion = mysqli_connect(HOST, USER, PASSWORD, DATABASE);
    if(mysqli_connect_errno()){
        echo "Fallo al conectar con la BBDD";
        exit();
    }
    
    
    /*Si el usuario tiene animales apadrinados que salga su foto, sino que salga el mensaje*/
    $consulta1= "SELECT foto FROM apadrinados WHERE padrino='".$reg["dni"]."'";
    $resultado1=mysqli_query($conexion, $consulta1);
    
    if (mysqli_num_rows($resultado1) > 0) {
        while($fila=mysqli_fetch_row($resultado1)){
            $resultadoSQL[]=$fila;
        }
        
       
    }else{
        $mensaje="Todavia no has apadrinado ningun animal.";
    }
    
    
    ?>
    <section class="container" id="cuerpo">

        <!-- Imagen cabecera -->
        <section class="container slider" id="cabecera1"></section>

        <!--MENU-->
        <nav class="navbar navbar-light navbar-expand-sm sticky-top" style="background-color: #0F7304;">

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                <div class="navbar-nav mr-auto ml-auto text-center" id="enlaces">
                    <a class="nav-item nav-link" href="index.php">INICIO</a>
                    <a class="nav-item nav-link" href="animales.php">ANIMALES</a>
                    <a class="nav-item nav-link active" href="frontend.php">USUARIOS</a>

                </div>
            </div>
        </nav>

        <!--CUERPO DE LA PAGINA-->

        <h1>
            <p class="mt-5 text-center font-weight-bold "><?php echo "¡Hola ".$reg['nombre']."!"; ?></p>
        </h1>
        <div class="container">
            <div class="row">
                
                    <!--Tabla que muestra los datos de perfil del usuario-->
                    <table class="table table-responsive" id="tabla">
                        <thead>
                            <tr>
                                <th colspan="5">DATOS PERFIL</th>
                            </tr>
                            <tr>
                                <th>DNI</th>
                                <th>NOMBRE</th>
                                <th>CORREO</th>
                                <th>TELEFONO</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            /*Coger los datos del usuario y mostrarlos en la tabla*/
                            $sql="SELECT * FROM usuarios WHERE dni='".$reg["dni"]."'";
                            $result=mysqli_query($conexion, $sql);
                            while($row=mysqli_fetch_row($result)){
                                echo "<tr>
                                <td>$row[0]</td>
                                <td>$row[1]</td>
                                <td>$row[2]</td>
                                <td>$row[4]</td>
                                </tr>";
                            
                            } 
                            
                            ?>
                        </tbody>

                    </table>
            </div>
            <!--Fila para ver los ahijados y para el boton y formulario de modificar datos perfil -->
            <div class="row">
                <div class="col-md mt-5 text-center">
                    <!--Ahijados del usuario-->
                    <h2 id="titulo">TUS AHIJADOS</h2>
                    <?php if(isset($resultadoSQL) && is_array($resultadoSQL)){ ?>
                    <?php foreach( $resultadoSQL as $linea ){ ?>
                    <img src="/OASIS/imagenes/<?php echo $linea[0];?>" width="50%" class="rounded-circle" />

                    <?php } ?>
                    <?php } ?>
                    <!--Sale este mensaje si todavia no ha apadrinado ningun animal-->
                    <?php if(isset($mensaje)){echo $mensaje;} ?>

                </div>
                <div class="col-md mt-5 text-center">
                    <!--Boton para modificar datos perfil-->
                    <button type="button" class="btn btn-success btn-lg" id="modificar" onClick="document.getElementById('mi-formulario').classList.remove('formoculto')">Modificar datos perfil</button>
                    <br><br><br>
                    <!--Formulario de modificar datos de perfil-->
                    <form method='post' action='' class='formoculto' id='mi-formulario'>
                        <h2 id="titulo">MODIFICACIONES</h2>
                        <label><strong>CORREO</strong></label><br>
                        <input type='text' name='correo' id="input"><br>
                        <label><strong>CONTRASEÑA</strong></label><br>
                        <input type='password' name='password' id="input" data-type="password"><br>
                        <label><strong>TELEFONO</strong></label><br>
                        <input type='text' name='telefono' id="input"><br><br>
                        <input type='submit' name='mod2' id="boton" value="ENVIAR">
                    </form>


                    <?php
                        if(isset($_POST["mod2"])){
                            if(isset($_POST["correo"]) && $_POST["correo"] != ""){
                                $query1 = "UPDATE usuarios SET correo='".$_POST["correo"]."' WHERE dni='".$reg["dni"]."'";
                                mysqli_query($conexion, $query1) or die(mysqli_error($conexion));
                                echo "Correo modificado correctamente.<br>";
                                
                            }
                            if(isset($_POST["password"]) && $_POST["password"] != ""){
                                $query2 = "UPDATE usuarios SET password='".$_POST["password"]."' WHERE dni='".$reg["dni"]."'";
                                mysqli_query($conexion, $query2) or die(mysqli_error($conexion));
                                echo "Contraseña modificada correctamente.<br>";
                                
                            }
                            if(isset($_POST["telefono"]) && $_POST["telefono"] != ""){
                                $query3 = "UPDATE usuarios SET telefono='".$_POST["telefono"]."' WHERE dni='".$reg["dni"]."'";
                                mysqli_query($conexion, $query3) or die(mysqli_error($conexion));
                                echo "Teléfono modificado correctamente.<br>";
                                
                            }
                            
                         
                        }
                    ?>

                </div>
            </div>
            <!--Fila para el boton de apadrinar aniamles-->
            <div class="row">
                <div class="col-md mt-5 mb-5" id="btnapadrinar">
                    <p>¿QUIERES APADRINAR UN ANIMAL?</p>
                    <button type="submit" onclick="location.href='apadrinar.php'" class="btn btn-success btn-lg">APADRINAR</button>
                </div>
            </div>
            <!--Fila para el enlace de Cerrar sesión-->
            <div class="row">
                <div class="col-md mt-5 mb-3 ml-3">
                    <strong><a href="cerrar.php" style="color:#0F7304;">Cerrar sesión</a></strong>
                </div>
            </div>
        </div>


        <!--FOOTER-->
        <div id="footer">
            <a href="https://es-es.facebook.com/"><img src="imagenes/faceicon.png" /></a>
            <a href="https://twitter.com/?lang=es"><img src="imagenes/twittericon.png" /></a>
            <a href="https://www.instagram.com/?hl=es"><img src="imagenes/instaicon.png" /></a>
        </div>


    </section>



    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>






</body>

</html>
