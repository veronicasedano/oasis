<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <title>OASIS - Apadrina</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="misEstilos.css" media="screen" />

</head>

<body>
    <?php
    session_start();
    include("datosconexion.php");
    $reg=$_SESSION['reg'];
    
/*--------------------------------------------------------------------*/    
    //CONEXION BASE DE DATOS
    $conexion = mysqli_connect(HOST, USER, PASSWORD, DATABASE);
    if(mysqli_connect_errno()){
        echo "Fallo al conectar con la BBDD";
        exit();
    }

/*--------------------------------------------------------------------*/
    //PARA MOSTRAR LA INFORMACION DE LOS ANIMALES
    $consulta1= "SELECT foto, nombre, id_animal FROM animales";
        $resultado1=mysqli_query($conexion, $consulta1);

        if (mysqli_num_rows($resultado1) > 0) {
            while($fila=mysqli_fetch_row($resultado1)){
                $resultadoSQL[]=$fila;
            }


        }else{
            $mensaje="Todavia no hay animales para apadrinar.";
        }
/*-----------------------------------------------------------------------------------------------------------------------*/
    
    
    if(isset($_POST['apadrinar1'])){
        //Compruebo si ese animal ya esta apadrinado por el usuario
        $sq1="SELECT nombre FROM apadrinados WHERE animal= '".$_POST['IDAnimal']."' AND padrino= '".$reg['dni']."' ";
        $re1=mysqli_query($conexion, $sq1);
        if ($re1 != false && mysqli_num_rows($re1) > 0) {
            $m1= "Ya tienes como ahijado este animal.";
            $animal_apadrinado = $_POST['IDAnimal'];
        }else{
            //Recojo el dinero que quiere donar y el id_animal
            $dinero= $_POST['dinero'];
            $id_animal = $_POST[ 'IDAnimal' ];

            //Hago consulta a la tabla animales para recoger los datos necesarios
            $sqlC="SELECT especie, nombre, sexo, foto FROM animales WHERE id_animal='".$_POST['IDAnimal']."'";
            $resultC=mysqli_query($conexion, $sqlC);
            while($filaC =mysqli_fetch_row($resultC)){
                $especie=$filaC[0];
                $nombre=$filaC[1];
                $sexo=$filaC[2];
                $foto=$filaC[3];
            }

            $queryC="INSERT INTO apadrinados VALUES (NULL, '$id_animal' , '".$reg['dni']."', '$nombre' , '$especie', '$sexo' , '$foto' , '$dinero')";
            mysqli_query($conexion, $queryC);
            $mensaje1= "¡Enhorabuena! Has apadrinado a este animal.";
            $animal_apadrinado = $id_animal;

            }    
        
  
    }
    
    
    ?>
<!----------------------------------------------------------------------------------------------------------------------->


    <section class="container" id="cuerpo">

        <!-- Imagen cabecera -->
        <section class="container slider" id="cabecera1"></section>

        <!--MENU-->
        <nav class="navbar navbar-light navbar-expand-sm sticky-top" style="background-color: #0F7304;">

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                <div class="navbar-nav mr-auto ml-auto text-center" id="enlaces">
                    <a class="nav-item nav-link" href="index.php">INICIO</a>
                    <a class="nav-item nav-link" href="animales.php">ANIMALES</a>
                    <a class="nav-item nav-link active" href="frontend.php">USUARIOS</a>

                </div>
            </div>
        </nav>

        <!--CUERPO DE LA PAGINA-->

        <h1>
            <p class="mt-5 text-center font-weight-bold "><?php echo "¡".$reg['nombre'].", anímate a apadrinar!"; ?></p>
        </h1>
        <div class="container mt-5">

            <div class="text-center" id="animales">
                <!--Mostrar foto y nombre de los animales-->
                <?php if(isset($resultadoSQL) && is_array($resultadoSQL)){ ?>
                <?php foreach( $resultadoSQL as $linea ){ ?>
                <img src="/oasis/imagenes/<?php echo $linea[0];?>" width="60%" /><br>
                <?php echo "<strong style='font-size:23px'>".$linea[1]."</strong>"; ?>
                <br><br>
                <!--Boton de apadrinar-->
                <button type="button" class="btn btn-success btn-lg" id="apadrinar" onClick="document.getElementById('formApadrinar_<?=$linea[2];?>').classList.remove('formoculto')">APADRINAR</button>
                <br><br>
                <form method="post" action="" id="formApadrinar_<?=$linea[2];?>" class="formoculto">
                    <label>¿Cuánto dinero quieres donar al mes?<br>
                        <input type="number" name="dinero">
                        <input type="submit" value="Enviar" name="apadrinar1">
                    </label>
                    <input type="hidden" name="IDAnimal" value="<?php echo $linea[2];?>"/>
                </form>
                <!--Mensaje para avisar de que ha apadrinado correctamente al animal -->
                <p><?php if(isset($mensaje1) && $animal_apadrinado == $linea[2]){echo $mensaje1;} ?></p>
                <!--Mensaje para avisar de que ya tiene este animal apadrinado -->
                <p><?php if(isset($m1) &&  $animal_apadrinado == $linea[2]){echo $m1;} ?></p>

                <hr noshade="noshade" style="width:100%"><br>

                <?php } ?>

                <?php } ?>

                <!--Sale este mensaje si todavia no hay animales en la BD-->
                <?php if(isset($mensaje)){echo $mensaje;} ?>

            </div>


            <!--Enlace para volver a la pagina frontend.php-->
            <div class="row">
                <div class="col-md mt-5 mb-3 ml-3">
                    <strong><a href="frontend.php" style="color:#0F7304;">Volver</a></strong>
                </div>
            </div>
        </div>


        <!--FOOTER-->
        <div id="footer">
            <a href="https://es-es.facebook.com/"><img src="imagenes/faceicon.png" /></a>
            <a href="https://twitter.com/?lang=es"><img src="imagenes/twittericon.png" /></a>
            <a href="https://www.instagram.com/?hl=es"><img src="imagenes/instaicon.png" /></a>
        </div>


    </section>



    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>






</body>

</html>
