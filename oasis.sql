-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-05-2020 a las 02:11:16
-- Versión del servidor: 10.1.35-MariaDB
-- Versión de PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `oasis`
--
CREATE DATABASE IF NOT EXISTS `oasis` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `oasis`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `animales`
--

CREATE TABLE IF NOT EXISTS `animales` (
  `id_animal` varchar(30) NOT NULL,
  `especie` varchar(30) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `sexo` varchar(30) NOT NULL,
  `fnto` date NOT NULL,
  `caracter` varchar(90) NOT NULL,
  `problemas` varchar(90) NOT NULL,
  `historia` varchar(300) NOT NULL,
  `foto` varchar(30) NOT NULL,
  PRIMARY KEY (`id_animal`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `animales`
--

INSERT INTO `animales` (`id_animal`, `especie`, `nombre`, `sexo`, `fnto`, `caracter`, `problemas`, `historia`, `foto`) VALUES
('111A', 'hamster', 'Calcetines', 'Macho', '2019-05-01', 'Es un poco gruñon pero un encanto cuando te conoce. Se lleva mal con otros hamsters.', 'Diabetes', 'Apareció en frente de una perrera metido en una caja de zapatos junto a 4 hermanos mas. Sus hermanos ya han sido adoptados pero él todavía no debido a su diabetes.', 'calcetines.png'),
('112B', 'hamster', 'Misty', 'Hembra', '2019-09-11', 'Es muy cariñosa y lista. Cuando quiere algo siempre encuentra la manera de decirtelo.', 'Ninguno', 'Fue rescatada de un criadero ilegal junto a 80 hamsters mas. Estos han sido llevamos a otros centros de adopciones y a diferentes casas de acogida. Estaba en pésimas condiciones de higiene sin comida y apenas agua. ', 'Misty.png'),
('113C', 'rata', 'Patrick', 'Macho', '2019-01-07', 'Muy dormilón. Le encantan las pipas de calabaza, el maíz y que le rasquen la barriga.', 'Obesidad', 'Estaba en una tienda de animales en la que nadie lo quería ni lo adoptaba. Los responsables de la tienda contactaron con nosotros para que nos hicieramos cargo de él. Lo habían estado alimentado con comida basura pero hoy en día ya esta a dieta con una alimentación sana y equilibrada.', 'patrick.png'),
('114D', 'rata', 'Lorie', 'Hembra', '2019-07-22', 'Es juguetona, lista y también le gusta robar comida.', 'Ninguno', 'Apareció en una cuneta de la carretera metida en una jaula muy pequeña con dos ratas mas. La encontró un transeúnte y este avisó a la policía la cual nos llamó a nosotros. Cuando la encontramos estaba desnutrida y con dermatitis. Pero por suerte ya esta totalmente recuperada.', 'lorie.png'),
('115E', 'conejo', 'Lilbe', 'Macho', '2019-03-16', 'Un poco asustadizo hasta que toma confianza. Se lleva muy bien con otros conejos.', 'Otitis del oído derecho', 'Lo encontramos en el jardín de una residencia de viviendas. Estaba solo, asustado y mojado. Imaginamos que se cayó a la piscina pero por suerte pudo salir él solo. Al principio nos costo bastante cogerle porque huía constantemente pero al final con un poco de comida acudió y lo pudimos rescatar.', 'lilbe.png'),
('116F', 'conejo', 'Fissy', 'Hembra', '2018-03-06', 'Es muy simpática y cariñosa con los niños especialmente. Le encantan las fresas.', 'Conjuntivitis', 'La rescatamos cuando una persona la vio correr en medio de la carretera y nos dio el aviso. Fue fácil recogerla porque acudía enseguida. Al principio pensamos que se había escapado y buscamos a sus respectivos humanos pero al no obtener respuesta ahora busca un hogar definitivo.', 'fissy.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `apadrinados`
--

CREATE TABLE IF NOT EXISTS `apadrinados` (
  `id_apadrinado` int(30) NOT NULL AUTO_INCREMENT,
  `animal` varchar(30) NOT NULL,
  `padrino` varchar(30) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `especie` varchar(30) NOT NULL,
  `sexo` varchar(30) NOT NULL,
  `foto` varchar(30) NOT NULL,
  `eurosMes` int(30) NOT NULL,
  PRIMARY KEY (`id_apadrinado`),
  KEY `animal` (`animal`),
  KEY `padrino` (`padrino`),
  KEY `animal_2` (`animal`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `apadrinados`
--

INSERT INTO `apadrinados` (`id_apadrinado`, `animal`, `padrino`, `nombre`, `especie`, `sexo`, `foto`, `eurosMes`) VALUES
(1, '111A', '82460013F', 'Calcetines', 'hamster', 'macho', 'calcetines.png', 10),
(2, '112B', '82460013F', 'Misty', 'hamster', 'hembra', 'Misty.png', 20),
(6, '111A', '76225443K', 'Calcetines', 'hamster', 'Macho', 'calcetines.png', 8),
(13, '112B', '76225443K', 'Misty', 'hamster', 'Hembra', 'Misty.png', 89),
(16, '114D', '76225443K', 'Lorie', 'rata', 'Hembra', 'lorie.png', 5),
(17, '115E', '76225443K', 'Lilbe', 'conejo', 'Macho', 'lilbe.png', 15),
(18, '116F', '76225443K', 'Fissy', 'conejo', 'Hembra', 'fissy.png', 3),
(19, '116F', '82460013F', 'Fissy', 'conejo', 'Hembra', 'fissy.png', 5),
(20, '114D', '45712239T', 'Lorie', 'rata', 'Hembra', 'lorie.png', 10),
(25, '116F', '45712239T', 'Fissy', 'conejo', 'Hembra', 'fissy.png', 10),
(33, '111A', '45712239T', 'Calcetines', 'hamster', 'Macho', 'calcetines.png', 40),
(34, '112B', '45712239T', 'Misty', 'hamster', 'Hembra', 'Misty.png', 50),
(38, '115E', '45712239T', 'Lilbe', 'conejo', 'Macho', 'lilbe.png', 34),
(41, '113C', '45712239T', 'Patrick', 'rata', 'Macho', 'patrick.png', 123),
(44, '113C', '82460013F', 'Patrick', 'rata', 'Macho', 'patrick.png', 30),
(47, '111A', '12345678J', 'Calcetines', 'hamster', 'Macho', 'calcetines.png', 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `dni` varchar(30) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `correo` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `telefono` varchar(9) NOT NULL,
  `rol` varchar(30) NOT NULL,
  PRIMARY KEY (`dni`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`dni`, `nombre`, `correo`, `password`, `telefono`, `rol`) VALUES
('12345678J', 'Laura', 'laura@gmail.com', '123', '658210374', 'usuario'),
('45712239T', 'Andrea', 'andrea@gmail.com', '123', '699741350', 'usuario'),
('76225443K', 'Juan', 'juan2@gmail.com', '123', '682100793', 'usuario'),
('78996864Y', 'Veronica', 'veronica@gmail.com', '123', '685201146', 'admin'),
('82460013F', 'Mario', 'mario2@gmail.com', '123', '691038421', 'usuario');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `apadrinados`
--
ALTER TABLE `apadrinados`
  ADD CONSTRAINT `apadrinados_ibfk_1` FOREIGN KEY (`padrino`) REFERENCES `usuarios` (`dni`),
  ADD CONSTRAINT `apadrinados_ibfk_2` FOREIGN KEY (`animal`) REFERENCES `animales` (`id_animal`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
